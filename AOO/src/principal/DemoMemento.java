package principal;

import memento.CareTaker;
import memento.Originator;

public class DemoMemento {

    public static void main(String[] args) {

        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();

        originator.setEstado("Novo Cadastro");
        careTaker.add((originator.salvarEstadoMemento()));

        originator.setEstado("Cadastro Andamento");
        careTaker.add(originator.salvarEstadoMemento());

        originator.setEstado("Cadastro Finalizado");
        careTaker.add(originator.salvarEstadoMemento());

        originator.setEstado("Cadastro Excluido");
        careTaker.add(originator.salvarEstadoMemento());

        originator.setEstado("Estado atual");
        System.out.println("Estado atual: " +originator.getEstado());

        originator.getEstadoSalvoMemento(careTaker.get(3));
        System.out.println("Estado salvo: " + originator.getEstado());




    }
}
