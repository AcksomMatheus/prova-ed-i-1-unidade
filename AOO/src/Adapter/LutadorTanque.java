/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author cjfac
 */
public class LutadorTanque implements Lutador {

    public void nome() {
        System.out.println("Lutador Tanque");
    }

    @Override
    public void combo() {
        System.out.println("Combo: Soco, Soco, Chute");
    }

    @Override
    public void especial() {
        System.out.println("Epecial: Escudo Inquebravel");
    }

      @Override
    public void atordoamento() {
        System.out.println("Atoroda seu inimigo");
    }
}
