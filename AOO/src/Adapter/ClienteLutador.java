/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author cjfac
 */
public class ClienteLutador {
    public static void main(String[] args) {
        LutadorTanque lutador = new LutadorTanque();
        MagoAssassino mago = new MagoAssassino();
        
        MagoAdapter  magoAdapter = new MagoAdapter(mago);
        Lutador[] lutadores = {lutador, magoAdapter};
        for( Lutador p : lutadores ){
            p.nome();
            p.combo();
            p.especial();
            
           p.atordoamento();
            System.out.println();
       
        }
    }
    
}
