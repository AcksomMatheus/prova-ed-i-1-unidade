/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author cjfac
 */
public class MagoAssassino implements Mago {

    public void nome() {
        System.out.println("Mago Assassino");
    }

    @Override
    public void magia() {
        System.out.println("Magia: Obliterar");
    }

    @Override
    public void especial() {
        System.out.println("Especial: Redenção");
    }

}
