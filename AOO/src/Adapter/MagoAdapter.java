/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Adapter;

/**
 *
 * @author cjfac
 */
public class MagoAdapter implements Lutador{
    
    private Mago mago;
    
    public MagoAdapter(Mago mago){
        this.mago = mago;
    }

    @Override
    public void combo() {
        mago.magia(); /*Metodo 'Magia' de Mago sendo usado de forma adaptada*/
    }

    @Override
    public void especial() {
        mago.especial(); /*Metodo 'Especial' usando de forma adaptada*/
    }

    @Override
    public void nome() {
       mago.nome();
       /* System.out.println(", Apatado para ser Lutador");*/
    }

   @Override
    public void atordoamento() {
        System.out.println("Mago nao possue essa habilidade");
    }
    
}
