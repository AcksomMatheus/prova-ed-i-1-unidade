
import javax.swing.*;

public class LS implements TL {

    No inicio, fim;
    int totalNos;

    public LS() {
        inicio = fim = null;
        totalNos = 0;
    }

    public int getTotalNos() {
        return totalNos;
    }

    @Override
    public boolean estaVazia() {
        if (getTotalNos() == 0) {
            return true;
        }
        return false;
    }

    @Override
    public void adicionarInicio(No n) {
        if (estaVazia()) {
            inicio = fim = n;
        } else {
            n.prox = inicio;
            inicio = n;
        }
        totalNos++;
    }

    public void adicionarMeio(No n, No p) {
        if(p.prox == null){
            fim = n;            
        }
        n.prox = p.prox;
        p.prox = n;
        totalNos++;
    }

    @Override
    public void adicionarFim(No n) {
        if (estaVazia()) {
            inicio = fim = n;
        } else {
            fim.prox = n;
            fim = n;
        }
        totalNos++;
    }

    @Override
    public void remover(No n) {
        No noAtual;
        No noAnterior;
        noAtual = noAnterior = inicio;
        int contador = 1;

        if (estaVazia() == false) {
            while (contador <= getTotalNos() && noAtual.dado != n.dado) {
                noAnterior = noAtual;
                noAtual = noAtual.prox;
                contador++;
            }

            if (noAtual.dado == n.dado) {
                if (getTotalNos() == 1) {
                    inicio = fim = null;
                } else {
                    if (noAtual == inicio) {
                        inicio = noAtual.prox;
                    } else {
                        noAnterior.prox = noAtual.prox;
                    }
                }
                totalNos--;
            }
        }
    }

    @Override
    public void listar() {
        listarAux(inicio);
    }
    
    private void listarAux(No p) {
        if (p != null) {
            System.out.println(p.dado);
            listarAux(p.prox);
        }
    }
}
