

package ProvaPrimeiraUni;

/**
 *
 * @author Acksom Matheus
 */
public class Main {
    public static void main(String[] args) throws Exception {
        ListaEncadeada l = new ListaEncadeada();
        No n1 = new No(1);
        No n2 = new No(2);
        No n3 = new No(3);
        No n4 = new No(4);
        l.adicionarInicio(n1);
        l.adicionarFim(n2);
        l.adicionarMeio(n3, n2);
        l.adicionarFim(n4);
      

        l.dividirLista(l, 2);
    }

}
