package ProvaPrimeiraUni;

/**
 *
 * @author Acksom Matheus
 */
public interface ILista{

    public ILista[] dividirLista(ILista original, int posicao) throws Exception;

    public boolean estaVazia();

    public void adicionarInicio(No n);

    public void adicionarFim(No n);
    
    public void adicionarMeio(No n, No p);

    public void remover(No n);

    public void listar();

}
