public class No {

    Object dado;
    No prox;

    public No(Object dado) {
        this.dado = dado;
        this.prox = null;
    }

    public Object getDado() {
        return dado;
    }

    public No getProx() {
        return prox;
    }

    public void setProx(No prox) {
        this.prox = prox;
    }
}
