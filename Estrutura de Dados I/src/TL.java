public interface TL {
     
   public boolean estaVazia();
   public void adicionarInicio(No n);
   public void adicionarFim(No n);
   public void remover(No n);
   public void listar();
    
    
}
