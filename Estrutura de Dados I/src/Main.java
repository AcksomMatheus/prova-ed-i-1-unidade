/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author cjfac
 */
public class Main {
    public static void main(String[] args) {
        LS l = new LS();
        No n2 = new No(2);
        l.adicionarFim(new No(1));
        l.adicionarInicio(new No(1));
        
        l.adicionarFim(n2);
        l.adicionarMeio(new No(3), n2);
        
        l.adicionarInicio(new No(5));
        l.adicionarFim(new No(6));
        l.adicionarFim(new No(7));


        l.listar();

        l.remover(new No(5));
        System.out.println("Removido");
        l.listar();
    }
    
}
